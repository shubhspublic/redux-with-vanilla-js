const redux = require("redux");
const reduxLogger = require("redux-logger"); // --> this will log all actions with state of store
const axios = require("axios");
const thunkMiddleware = require("redux-thunk").default; // --> redux thunk allows action to be asynchronous i.e. api request(not-pure)

const createStore = redux.createStore;
const applyMiddleware = redux.applyMiddleware;
const logger = new reduxLogger.createLogger();

const intialState = {
  loading: false,
  users: [],
  error: "",
};

//Action-type
const FETCH_USERS_REQUEST = "FETCH_USERS_REQUEST";
const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
const FETCH_USERS_FAILURE = "FETCH_USERS_FAILURE";

//Action-Creators
const fetchUsersRequest = () => {
  return {
    type: FETCH_USERS_REQUEST,
  };
};

const fetchUsersSuccess = users => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users,
  };
};

const fetchUsersFailure = error => {
  return {
    type: FETCH_USERS_FAILURE,
    payload: error,
  };
};

const fetchUsers = () => {
  return function (dispatch) {
    //before making api-call, may be to show loader, dispatch FETCH_USERS_REQUEST
    dispatch(fetchUsersRequest());

    //making api-call
    axios
      .get("https://jsonplaceholder.typicode.com/users") // fake-free api
      .then((response) => {
        //here, response.data is an array of users
        const users = response.data.map(user => user.id);
        dispatch(fetchUsersSuccess(users));
      })
      .catch((error) => {
        //here, error.message gives the description of the error
        dispatch(fetchUsersFailure(error.message));
      });
  };
};

//Reducers
const reducer = (state = intialState, action) => {
  switch (action.type) {
    case FETCH_USERS_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        users: action.payload,
        error: "",
      };

    case FETCH_USERS_FAILURE:
      return {
        ...state,
        users: [],
        error: action.payload,
      };

    default:
      return {
        state,
      };
  }
};

//Creata a middlewares array, if you have multiple arguments to pass as middlewares
const middlewares = [thunkMiddleware, logger];

//Create Store
const store = createStore(reducer, applyMiddleware(...middlewares));

//subscribing to store
const unsubscribe = store.subscribe(() => {
    //we can console.log here but here We are already using redux-logger which will log all the action for us
    //console.log("Updated-State --> ", store.getState());
});

//Calling dispath method in order to fetch users using api
store.dispatch(fetchUsers())

//if we want to unsubscribe from store then
//unsubscribe()

